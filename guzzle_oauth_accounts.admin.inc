<?php

/**
 * Menu callback
 * Form to select the entity bundles where an account can attach to.
 */
function guzzle_oauth_accounts_configure($form, &$form_state) {
  $entities = entity_get_info();
  $bundles = array();
  $form['bundles'] = array(
    '#tree' => TRUE,
  );
  $form['bundles']['form_explanation_helper'] = array(
    '#markup' => t('Select the entity bundles where Oauth Accounts can be attached to.'),
  );
  $values = variable_get('guzzle_oath_account_attach_entities', array());
  foreach ($entities as $entity_type => $entity_info) {
    // Check if we have something to attach.
    if (!isset($entity_info['uri callback']) || !count($entity_info['bundles']) || !isset($entity_info['form callback'])) {
      continue;
    }
    $form['bundles'][$entity_type] = array(
      '#type' => 'fieldset',
      '#title' => t($entities[$entity_type]['label']),
    );
    foreach ($entity_info['bundles'] as $bundle_key => $bundle_info) {
      $form['bundles'][$entity_type][$bundle_key] = array(
        '#type' => 'checkbox',
        '#title' => t($bundle_info['label']),
        '#default_value' => (bool)(isset($values[$entity_type]) && in_array($bundle_key, $values[$entity_type])),
      );
    }

  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit form callback.
 */
function guzzle_oauth_accounts_configure_submit(&$form, &$form_state) {
  $values = &$form_state['values'];
  $save = array();
  foreach ($values['bundles'] as $entity => $bundles) {
    foreach ($bundles as $bundle => $value) {
      if ($value) {
        $save[$entity][] = $bundle;
      }
    }
  }
  variable_set('guzzle_oath_account_attach_entities', $save);
  // We need to rebuild the menu, to insert the new items in hook_menu.
  menu_rebuild();
  drupal_set_message(t('The configuration options have been saved.'));
}
