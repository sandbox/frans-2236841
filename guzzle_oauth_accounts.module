<?php

/**
 * Implements HOOK_menu().
 */
function guzzle_oauth_accounts_menu() {
  $items = array();
  $items['admin/config/services/guzzle-oauth-consumers/accounts'] = array(
    'title' => 'Oauth Accounts',
    'description' => 'Configure Oauth Accounts.',
    'page callback' => 'guzzle_oauth_remote_accounts_entity_overview',
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth_accounts.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/services/guzzle-oauth-consumers/accounts/overview'] = array(
    'title' => 'Oauth Accounts Overview',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  $items['admin/config/services/guzzle-oauth-consumers/accounts/add'] = array(
    'title' => 'Add Oauth account',
    'description' => 'Add Oauth account.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guzzle_oauth_remote_accounts_entity_add'),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth_accounts.pages.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/services/guzzle-oauth-consumers/accounts/%'] = array(
    'title' => 'Delete Oauth account',
    'description' => 'Delete Oauth account.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guzzle_oauth_remote_accounts_entity_delete', 6),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth_accounts.pages.inc',
  );
  $items['admin/config/services/guzzle-oauth-consumers/accounts/configure'] = array(
    'title' => 'Oauth Accounts Configure',
    'description' => 'Configure Oauth Accounts.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guzzle_oauth_accounts_configure'),
    'access arguments' => array('administer site configuration'),
    'file' => 'guzzle_oauth_accounts.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  // Attach to accounts.
  $attach_to = variable_get('guzzle_oath_account_attach_entities', array());
  foreach ($attach_to as $entity_type => $bundles) {
    $info = entity_get_info($entity_type);
    $entity = new stdClass();
    $entity->{$info['entity keys']['id']} = '%' . $entity_type;
    $path = $info['uri callback']($entity);
    $parts = array_flip(explode('/', $path['path']));
    $items[$path['path'] . '/remote-accounts'] = array(
      'title' => 'Remote accounts',
      'description' => 'Configure remote accounts.',
      'page callback' => 'guzzle_oauth_remote_accounts_entity_overview',
      'page arguments' => array($parts['%' . $entity_type], $entity_type),
      'access callback' => 'guzzle_oauth_remote_accounts_entity_overview_access_callback',
      'access arguments' => array($parts['%' . $entity_type], $entity_type),
      'file' => 'guzzle_oauth_accounts.pages.inc',
      'type' => MENU_LOCAL_TASK,
    );
    $items[$path['path'] . '/remote-accounts/add'] = array(
      'title' => 'Add remote account',
      'description' => 'Add remote account.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('guzzle_oauth_remote_accounts_entity_add', $parts['%' . $entity_type], $entity_type),
      'access callback' => 'guzzle_oauth_remote_accounts_entity_overview_access_callback',
      'access arguments' => array($parts['%' . $entity_type], $entity_type),
      'file' => 'guzzle_oauth_accounts.pages.inc',
      'type' => MENU_LOCAL_ACTION,
    );
    $items[$path['path'] . '/remote-accounts/delete/%'] = array(
      'title' => 'Delete remote account',
      'description' => 'Delete remote account.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('guzzle_oauth_remote_accounts_entity_delete', ($parts['%' . $entity_type] + 3),  $parts['%' . $entity_type], $entity_type),
      'access callback' => 'guzzle_oauth_remote_accounts_entity_overview_access_callback',
      'access arguments' => array($parts['%' . $entity_type], $entity_type),
      'file' => 'guzzle_oauth_accounts.pages.inc',
    );
  }
  return $items;
}

/**
 * Implements HOOK_permission().
 */
function guzzle_oauth_accounts_permission() {
  return array(
    'use global guzzle oauth accounts' => array(
      'title' => t('Use global guzzle oauth accounts'),
      'description' => t('Access to the global guzzle oauth accounts.'),
    ),
  );
}

/**
 * Access callback.
 * Can the overview page tab be opened?
 */
function guzzle_oauth_remote_accounts_entity_overview_access_callback($entity, $entity_type) {
  $attach_to = variable_get('guzzle_oath_account_attach_entities', array());
  if (!isset($attach_to[$entity_type])) {
    return FALSE;
  }
  $info = entity_get_info($entity_type);
  if (isset($info['entity keys']['bundle']) && $info['entity keys']['bundle']) {
    // This is not a selected bundle.
    if (!in_array($entity->{$info['entity keys']['bundle']}, $attach_to[$entity_type])) {
      return FALSE;
    }
  }
  return entity_access('update', $entity_type, $entity);
}

/**
 * Helper function for other modules to load all accounts an account has access to.
 */
function guzzle_oauth_accounts_by_user($consumer_instance = NULL, $consumers = NULL, $account = NULL) {
  global $user;
  if (!is_array($consumers) && !empty($consumers)) {
    $consumers = array($consumers);
  }
  if (!is_array($consumer_instance) && !empty($consumer_instance)) {
    $consumer_instance = array($consumer_instance);
  }
  if (empty($account)) {
    $account = $user;
  }
  $global_access = user_access('use global guzzle oauth accounts', $account);
  $query = db_select('guzzle_oauth_accounts', 'a')
    ->fields('a');
  $query->leftJoin('guzzle_oauth_consumer_instance', 'i', 'i.id = a.consumer_instance_id');
  $query->addField('i', 'title', 'consumer_instance_title');
  $query->addField('i', 'consumer');
  if (!empty($consumers)) {
    $query->condition('i.consumer', $consumers, 'IN');
  }
  if (!empty($consumer_instance)) {
    $query->condition('i.id', $consumer_instance, 'IN');
  }
  $result = $query->execute();
  $remote_accounts = array();
  $done = array();
  foreach ($result as $remote_account) {
    $identifier = $remote_account->consumer . '_' . $remote_account->remote_account_id;
    if (in_array($identifier, $done)) {
      continue;
    }
    if ($remote_account->entity_type == '' && $global_access) {
      $remote_account->access_token = unserialize($remote_account->access_token);
      $remote_accounts[] = $remote_account;
      $done[] = $identifier;
      continue;
    }
    // Unfortunately we need an entity_load.
    $entity = entity_load_single($remote_account->entity_type, $remote_account->entity_id);
    if (entity_access('update', $remote_account->entity_type, $entity, $account)) {
      $remote_account->access_token = unserialize($remote_account->access_token);
      $remote_accounts[] = $remote_account;
      $done[] = $identifier;
    }
  }
  return $remote_accounts;

}
