<?php

/**
 * Menu callback
 * Present a table with all attached accounts.
 */
function guzzle_oauth_remote_accounts_entity_overview($entity = NULL, $entity_type = '') {
  $build = array();
  if (empty($entity)) {
    $keys = array(0);
    $build['explain'] = array(
      '#markup' => '<p>' . t('Accounts added here are accesible for users with the permission !link. Accounts attached to an entity are accessible for users with update/edit permissions on that entity.', array(
        '!link' => l(t('Use global guzzle oauth accounts'), 'admin/people/permissions', array('fragment' => 'module-guzzle_oauth_accounts')),
      )) . '</p>',
    );
  } else {
    $keys = entity_extract_ids($entity_type, $entity);
  }
  $query = db_select('guzzle_oauth_accounts', 'a')
    ->fields('a', array('id', 'remote_account_label', 'remote_account_type', 'expires'))
    ->condition('a.entity_type', $entity_type)
    ->condition('a.entity_id', $keys[0]);
  $query->leftJoin('guzzle_oauth_consumer_instance', 'i', 'i.id = a.consumer_instance_id');
  $query->addField('i', 'title');
  $result = $query->execute();
  $rows = array();
  foreach ($result as $record) {
    $row = array(
      $record->remote_account_label,
      $record->remote_account_type,
      $record->title,
      empty($record->expires)?t('never'):format_date($record->expires),
      l('delete', $_GET['q'] . '/delete/' . $record->id, array('query' => drupal_get_destination())),
    );
    $rows[] = $row;
  }
  $header = array(t('Account'), t('Account type'), t('Service'), t('Access expires'), t('Operations'));
  $build['table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
    '#empty' => t('No remote accounts found.'),
  );
  return $build;
}

function guzzle_oauth_remote_accounts_entity_delete($form, &$form_state, $remote_account_id, $entity = NULL, $entity_type = '') {
  $query = db_select('guzzle_oauth_accounts', 'a')
    ->fields('a')
    ->condition('a.id', $remote_account_id);
  $query->leftJoin('guzzle_oauth_consumer_instance', 'i', 'i.id = a.consumer_instance_id');
  $query->addField('i', 'title');
  $result = $query->execute()->fetch();
  $form['#remote_account'] = $result;
  $form['remote_account_id'] = array(
    '#type' => 'value',
    '#value' => $remote_account_id,
  );
  return confirm_form($form, t('Are you sure you want to delete remote %service %type account %name?', array('%type' => $result->remote_account_type, '%service' => $result->title, '%name' => $result->remote_account_label)), drupal_get_destination());
}

function guzzle_oauth_remote_accounts_entity_delete_submit($form, &$form_state) {
  $values = &$form_state['values'];
  if ($values['confirm']) {
    db_delete('guzzle_oauth_accounts')
      ->condition('id', $values['remote_account_id'])
      ->execute();
    drupal_set_message(t('The remote %service %type account %name has been deleted.', array('%type' => $form['#remote_account']->remote_account_type, '%service' => $form['#remote_account']->title, '%name' => $form['#remote_account']->remote_account_label)));
  }
}

function guzzle_oauth_remote_accounts_entity_add($form, &$form_state, $entity = NULL, $entity_type = '', $selected_consumer_instance = NULL) {
  // Show list of consumers you can attach to.
  ctools_include('plugins');
  $consumers = ctools_get_plugins('guzzle_oauth', 'guzzle_oauth_consumers');
  foreach ($consumers as $name => $consumer) {
    if (!call_user_func_array($consumer['access callback'], $consumer['access arguments'])) {
      unset($consumers[$name]);
    }
  }
  $query = db_select('guzzle_oauth_consumer_instance', 'c');
  $query->fields('c', array('title', 'consumer', 'id'));
  $query->condition('c.status', 0, '<>');
  $consumer_instances = $query->execute()->fetchAll();
  if (empty($entity_type)) {
    $keys = array(0);
    //Shift arguments;
    $selected_consumer_instance = $entity;
    $entity = NULL;
  }
  else {
    $keys = entity_extract_ids($entity_type, $entity);
  }
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );
  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $keys[0],
  );

  $options = array();
  foreach ($consumer_instances as $instance) {
    if (isset($consumers[$instance->consumer])) {
      $options[$instance->id] = t($instance->title);
    }
  }
  $form['#consumer_instances'] = $options;

  $form['actions'] = array('#type' => 'actions');
  if (empty($selected_consumer_instance)) {
    $form['state'] = array(
      '#type' => 'value',
      '#value' => 'authorize',
    );
    $form['consumer_instance'] = array(
      '#type' => 'radios',
      '#title' => t('Select a service'),
      '#options' => $options,
      '#required' => TRUE,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Authorize remote account'),
    );
  } else {
    if (isset($_SESSION['GO_' . $selected_consumer_instance . '_ACCESS_TOKEN'])) {
      $_SESSION['tmp_accounts'] = array();
      $access_token = unserialize($_SESSION['GO_' . $selected_consumer_instance . '_ACCESS_TOKEN']);
      $client = guzzle_oauth_get_client_by_instance($selected_consumer_instance, $access_token);
      $accounts = $client->getConnectedAccounts();
      foreach ($accounts as $key => $account) {
        $accounts[$key]['consumer_instance_id'] = $selected_consumer_instance;
        if (!isset($account['access_token'])) {
          $accounts[$key]['access_token'] = $access_token;
        }
        if (!isset($account['expires'])) {
          $accounts[$key]['expires'] = 0;
          if (isset($access_token['expires_at'])) {
            $accounts[$key]['expires'] = $access_token['expires_at'];
          }
        }
        $accounts['a'.$key] = $accounts[$key];
        unset($accounts[$key]);
      }
      unset($_SESSION['GO_' . $selected_consumer_instance . '_ACCESS_TOKEN']);
      $_SESSION['tmp_accounts'] = $accounts;
    }
    $options = array();
    foreach ($_SESSION['tmp_accounts'] as $key => $account) {
      $options[$key] = $account['account_label'];
    }
    $form['state'] = array(
      '#type' => 'value',
      '#value' => 'confirm',
    );
    $form['consumer_instance'] = array(
      '#type' => 'value',
      '#value' => $selected_consumer_instance,
    );
    $form['connect_account'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => t('Confirm and select the account you want to attach.'),
      '#default_value' => array('a0'),
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Confirm'),
    );
    // We substract the last 2 slashes from the current url (add/%consumer_instance)
    // We do that here so that a form_alter can easily change the redirect.
    $lastp = substr($_GET['q'], 0, strrpos($_GET['q'], '/'));
    $form['#after_submit_redirect'] = url(substr($lastp, 0, strrpos($lastp, '/')));

  }
  $form['actions']['cancel'] = array(
    '#theme' => 'link',
    '#text' => t('Cancel'),
    '#path' => substr($_GET['q'], 0, strrpos($_GET['q'], '/')),
    '#options' => array('html' => FALSE, 'attributes' => array()),
  );
  return $form;
}

function guzzle_oauth_remote_accounts_entity_add_submit(&$form, &$form_state) {
  if ($form_state['values']['state'] == 'authorize') {
    $url = array(
      'path' => 'go/authorize/' . $form_state['values']['consumer_instance'],
      'options' => array(
        'query' => array('destination' => $_GET['q'] . '/' . $form_state['values']['consumer_instance']),
      ),
    );
    $form_state['redirect'] = array($url['path'], $url['options']);
  } elseif ($form_state['values']['state'] == 'confirm') {
    $accounts = $_SESSION['tmp_accounts'];
    unset($_SESSION['tmp_accounts']);

    $form_state['redirect'] = $form['#after_submit_redirect'];
    $form_state['storage']['save'] = array();
    foreach ($form_state['values']['connect_account'] as $key => $value) {
      if ($value == $key && $value) {
        $account = $accounts[$key];
        $result = db_select('guzzle_oauth_accounts', 'a')
          ->fields('a', array('id', 'access_token'))
          ->condition('remote_account_id', $account['account_id'])
          ->condition('remote_account_type', $account['account_type'])
          ->condition('consumer_instance_id', $form_state['values']['consumer_instance'])
          ->condition('entity_type', $form_state['values']['entity_type'])
          ->condition('entity_id', $form_state['values']['entity_id'])
          ->execute();
        $id = NULL;
        $existing_access_token = array();
        if ($row = $result->fetch()) {
          $id = $row->id;
          $existing_access_token = unserialize($row->access_token);
          if (empty($existing_access_token)) {
            $existing_access_token = array(); //ensure array, otherwise merge will fail
          }
        }
        // Merge token (old token can have more info, for example a refresh token)
        $save_token = array_merge($existing_access_token, $account['access_token']);
        // Get $expires either from the client or from the token.
        $expires = isset($connected_account['expires'])?$connected_account['expires']:0;
        if (!$expires && isset($save_token['expires_at'])) {
          $expires = $save_token['expires_at'];
        }
        $save = array(
          'id' => $id,
          'consumer_instance_id' => $form_state['values']['consumer_instance'],
          'remote_account_id' => $account['account_id'],
          'remote_account_label' => $account['account_label'],
          'remote_account_type' => $account['account_type'],
          'access_token' => $save_token,
          'expires' => $expires,
          'entity_type' => $form_state['values']['entity_type'],
          'entity_id' => $form_state['values']['entity_id'],
        );
        $pk = array();
        if ($id) {
          $pk[] = 'id';
        }
        drupal_write_record('guzzle_oauth_accounts', $save, $pk);
        $form_state['storage']['save'][] = $save;
        drupal_set_message(t('The remote %service %type account %name has been added.', array('%type' => $account['account_type'], '%service' => $form['#consumer_instances'][$form_state['values']['consumer_instance']], '%name' => $account['account_label'])));
      }
    }
  }
}
